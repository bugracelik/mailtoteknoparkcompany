package team.pairfy.mailtoteknoparkcompany;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SendMailTest {

    @Test
    void get_doc_title_from_url() throws IOException {
        String htmlUrl = "www.google.com";

        final String BASE_URL = "https://www.ariteknokent.com.tr/tr";
        List<String> allLinksOfCompanies = new ArrayList<>();

        Document doc = Jsoup.connect("https://www.ariteknokent.com.tr/tr/teknoloji-firmalari/teknokentli-firmalar").get();
        int sizeOfCompany = doc.getElementById("example").getElementsByTag("a").size(); // 588

        for (int i = 0; i <  sizeOfCompany; i += 2) {
            String firmaLink = doc.getElementById("example").getElementsByTag("a").get(i).attributes().get("href");
            String exactMatchingForLink = BASE_URL + firmaLink;
            allLinksOfCompanies.add(exactMatchingForLink);
        }


        FileOutputStream fos = new FileOutputStream(new File("./mails.txt"), true);
        List<String> companyEmails = new ArrayList<>();
        for (String companyLink : allLinksOfCompanies) {

            try {
                Document docLinkForOneCompany = Jsoup.connect(companyLink).get();
                String emailAdress = docLinkForOneCompany.getElementsByClass("company_address").get(0).text().split("E:")[1];
                fos.write(emailAdress.getBytes());
                fos.write("\n".getBytes());
                // companyEmails.add(emailAdress);
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        System.out.println(companyEmails);


        // send mail via api
        // SpringMailParser.send("mrbugracelik@gmail.com", companyEmails.get(0), "mail-title", "bodyOfMail");


        // new Scanner(System.in).nextLine();
        System.out.println("a");
        // toplandı


        // Elements odd = doc.get("odd");
        // System.out.println(odd);

        /*
        Elements newsHeadlines = doc.select("#mp-itn b a");
        for (Element headline : newsHeadlines) {
            System.out.println();
            log("%s\n\t%s",
                    headline.attr("title"), headline.absUrl("href"));
        }*/
    }

    // doc.getElementById("example").getElementsByTag("a").get(10).attributes.vals[0]


    @Test
    void rest() throws IOException {
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> response = restTemplate.getForEntity("https://www.google.com", String.class);

        String bodyHtml = response.getBody();

        FileOutputStream fos = new FileOutputStream(new File("./bugra.java"));
        fos.write(bodyHtml.getBytes());


        System.out.println(bodyHtml);

    }


    @Test
    void dosya_string_yazmaca() throws IOException {

        File foo = new File("/Users/tugberk.celik/Desktop/BUGRA-PAİRFY-TEAM/repositories/send-mail-to-teknopark-company/src/test/java/team/pairfy/mailtoteknoparkcompany/SendMailTest.java");
        String sendMailJava = new String(Files.readAllBytes(foo.toPath()));

        FileOutputStream fos = new FileOutputStream(new File("./tugberk.html"));
        fos.write(sendMailJava.getBytes());
    }
}

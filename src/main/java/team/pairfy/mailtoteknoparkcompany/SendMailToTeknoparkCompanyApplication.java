package team.pairfy.mailtoteknoparkcompany;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SendMailToTeknoparkCompanyApplication {

    public static void main(String[] args) {
        SpringApplication.run(SendMailToTeknoparkCompanyApplication.class, args);
    }

}

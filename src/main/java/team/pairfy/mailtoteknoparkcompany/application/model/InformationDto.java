package team.pairfy.mailtoteknoparkcompany.application.model;

public class InformationDto {
    private String message;

    public InformationDto(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "InformationDto{" +
                "message='" + message + '\'' +
                '}';
    }
}
